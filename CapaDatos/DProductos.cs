﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaDatos
{
    public class DProductos
    {
        public int Id_Producto { get; set; }

        public string Nombre_Producto { get; set; }

        public string Nombre_Categoria { get; set; }

        public decimal Precio_Unitario { get; set; }

        public string Detalles { get; set; }

        public string Nombre_Buscado { get; set; }

        public DProductos() { 
        
        }

        public DProductos(int parId_Producto, string parNombre_Producto, string parNombre_Categoria,
            decimal parPrecio_Unitario, string parDetalles)
        {
            this.Id_Producto = parId_Producto;
            this.Nombre_Producto = parNombre_Producto;
            this.Nombre_Categoria = parNombre_Categoria;
            this.Precio_Unitario = parPrecio_Unitario;
            this.Detalles = parDetalles;
        }

        public DataTable Mostrar(int regPag, int NumeroPagina)
        {
            var dtable = new DataTable("Produccion.ProductosMostrar");
            SqlConnection sqlConexion = new SqlConnection();

            try
            {
                sqlConexion.ConnectionString = DConexion.CnBDEmpresa;
                sqlConexion.Open();

                SqlCommand SqlComando = new SqlCommand();
                SqlComando.Connection = sqlConexion;

                SqlComando.CommandText = "Produccion.ProductosMostrar";
                SqlComando.CommandType = CommandType.StoredProcedure;

                SqlParameter RegistroporPagina = new SqlParameter();
                RegistroporPagina.ParameterName = "@RegistrosPorPagina";
                RegistroporPagina.SqlDbType = SqlDbType.Int;
                RegistroporPagina.Value = regPag;
                SqlComando.Parameters.Add(RegistroporPagina);

                SqlParameter numPaginas = new SqlParameter();
                numPaginas.ParameterName = "@NumeroPagina";
                numPaginas.SqlDbType = SqlDbType.Int;
                numPaginas.Value = NumeroPagina;
                SqlComando.Parameters.Add(numPaginas);

                SqlComando.ExecuteNonQuery();

                SqlDataAdapter adB = new SqlDataAdapter(SqlComando);
                adB.Fill(dtable);

            }
            catch (Exception ex)
            {
                dtable = null;
                throw new Exception("Error al intentar ejecutar el procedimiento alamacenado" + ex.Message);
            }
            finally {
                sqlConexion.Close();
            }
            return dtable;
        }

        public int TamanioPag(int regPag)
        {
            int totalPaginas = 1;
            SqlConnection sqlConexion = new SqlConnection();

            try
            {
                sqlConexion.ConnectionString = DConexion.CnBDEmpresa;
                sqlConexion.Open();

                SqlCommand SqlComando = new SqlCommand();
                SqlComando.Connection = sqlConexion;

                SqlComando.CommandText = "Produccion.TamanoProductos";
                SqlComando.CommandType = CommandType.StoredProcedure;

                SqlParameter RegistroporPagina = new SqlParameter();
                RegistroporPagina.ParameterName = "@RegistrosPorPagina";
                RegistroporPagina.SqlDbType = SqlDbType.Int;
                RegistroporPagina.Value = regPag;
                SqlComando.Parameters.Add(RegistroporPagina);

                SqlParameter parTotalPaginas = new SqlParameter();
                parTotalPaginas.ParameterName = "@TotalPaginas";
                parTotalPaginas.Direction = ParameterDirection.Output;
                parTotalPaginas.SqlDbType = SqlDbType.Int;
                SqlComando.Parameters.Add(parTotalPaginas);

                SqlComando.ExecuteNonQuery();

                totalPaginas = (int)SqlComando.Parameters["@TotalPaginas"].Value;

            }
            catch (Exception ex)
            {
                throw new Exception("Error al intentar ejecutar el procedimiento alamacenado" + ex.Message);
            }
            finally
            {
                sqlConexion.Close();
            }
            return totalPaginas;

        }

        public DataTable Buscar(DProductos productos)
        {
            var dtable = new DataTable("Produccion.ProductosMostrar");
            SqlConnection sqlConexion = new SqlConnection();

            try
            {
                sqlConexion.ConnectionString = DConexion.CnBDEmpresa;
                sqlConexion.Open();

                SqlCommand SqlComando = new SqlCommand();
                SqlComando.Connection = sqlConexion;

                SqlComando.CommandText = "Produccion.ProductosBuscar";
                SqlComando.CommandType = CommandType.StoredProcedure;

                SqlParameter nomBuscado = new SqlParameter();
                nomBuscado.ParameterName = "@NombreBuscado";
                nomBuscado.SqlDbType = SqlDbType.VarChar;
                nomBuscado.Size = productos.Nombre_Buscado.Length;
                nomBuscado.Value = productos.Nombre_Buscado;
                SqlComando.Parameters.Add(nomBuscado);

                SqlComando.ExecuteNonQuery();

                SqlDataAdapter adB = new SqlDataAdapter(SqlComando);
                adB.Fill(dtable);

            }
            catch (Exception ex)
            {
                dtable = null;
                throw new Exception("Error al intentar ejecutar el procedimiento alamacenado" + ex.Message);
            }
            finally
            {
                sqlConexion.Close();
            }
            return dtable;
        }

        public string Insertar(DProductos productos)
        {
            var respuesta = "";
            SqlConnection sqlConexion = new SqlConnection();

            try
            {
                sqlConexion.ConnectionString = DConexion.CnBDEmpresa;
                sqlConexion.Open();

                SqlCommand SqlComando = new SqlCommand();
                SqlComando.Connection = sqlConexion;

                SqlComando.CommandText = "Produccion.ProductosInsertar";
                SqlComando.CommandType = CommandType.StoredProcedure;

                SqlParameter parNombre_Producto = new SqlParameter();
                parNombre_Producto.ParameterName = "@Nombre_Producto";
                parNombre_Producto.SqlDbType = SqlDbType.VarChar;
                parNombre_Producto.Size = productos.Nombre_Producto.Length;
                parNombre_Producto.Value = productos.Nombre_Producto;
                SqlComando.Parameters.Add(parNombre_Producto);

                SqlParameter parNombre_Categoria = new SqlParameter();
                parNombre_Categoria.ParameterName = "@Nombre_Categoria";
                parNombre_Categoria.SqlDbType = SqlDbType.VarChar;
                parNombre_Categoria.Size = productos.Nombre_Categoria.Length;
                parNombre_Categoria.Value = productos.Nombre_Categoria;
                SqlComando.Parameters.Add(parNombre_Categoria);

                SqlParameter parPrecio_Unitario = new SqlParameter();
                parPrecio_Unitario.ParameterName = "@Precio_Unitario";
                parPrecio_Unitario.SqlDbType = SqlDbType.Money;
                parPrecio_Unitario.Value = productos.Precio_Unitario;
                SqlComando.Parameters.Add(parPrecio_Unitario);

                SqlParameter parDetalles = new SqlParameter();
                parDetalles.ParameterName = "@Detalles";
                parDetalles.SqlDbType = SqlDbType.VarChar;
                parDetalles.Size = productos.Detalles.Length;
                parDetalles.Value = productos.Detalles;
                SqlComando.Parameters.Add(parDetalles);

                SqlComando.ExecuteNonQuery();

                respuesta = "Y";

            }
            catch (SqlException ex)
            {
                if (ex.Number == 8152)
                {
                    respuesta = "has introducido demasiados caracteres en uno de los campos";
                }
                else if(ex.Number==2627){
                    respuesta = "Ya exister un producto con ese nombre";
                }
                else if (ex.Number == 515)
                {
                    respuesta = "No puedes dejar el campo Nombre de Producto o Nombre de Categoria vacio";
                }
                else if (ex.Number == 50000)
                {
                    respuesta = "El nombre de la categoria no existe.Si quieres creare una categoria nueva primero debes de crearla";
                }
                else {
                    throw new Exception("Error al intentar ejecutar el procedimiento alamacenado" + ex.Message);
                }
            }
            finally
            {
                sqlConexion.Close();
            }
            return respuesta;
        }

    }
}
