﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CapaNegocio;

namespace CapaPresentacion
{
    public partial class Productos : UserControl
    {
        int registroPorPagina = 25;
        int numeroPagina = 1;
        int cantidadPaginas;

        public Productos()
        {
            InitializeComponent();
            this.Dock = DockStyle.Fill;
            MostrarProd();
        }

        public void MostrarProd() {
            this.dataGridView.DataSource = NProductos.MostrarProd(registroPorPagina,numeroPagina);
            cantidadPaginas = NProductos.Tamanio(registroPorPagina);
            this.lblPaginacion.Text = string.Format("Pagina {0} de {1}", numeroPagina, cantidadPaginas);
        }

        public void BuscaProd() {
            try
            {
                this.dataGridView.DataSource = NProductos.BuscarProd(this.txtNombreBusc.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                throw;
            }
        }

        private void Refrescar() {
            this.numeroPagina = 1;
            this.MostrarProd();
            this.txtNombreBusc.Text = string.Empty;
        }

        private void btnPaginacionAtras_Click(object sender, EventArgs e)
        {
            if (numeroPagina > 1) {
                numeroPagina = numeroPagina - 1;
                MostrarProd();
            }
        }

        private void btnPaginacionSiguiente_Click(object sender, EventArgs e)
        {
            if (numeroPagina < cantidadPaginas) {
                numeroPagina = numeroPagina + 1;
                MostrarProd();
            }
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            FrmAgregarProducto AgregarProducto = new FrmAgregarProducto();
            AgregarProducto.ShowDialog();
        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            FrmEditarProducto EditarProducto = new FrmEditarProducto();
            EditarProducto.ShowDialog();
        }

        private void txtNombreBusc_TextChanged(object sender, EventArgs e)
        {
            if (this.txtNombreBusc.Text == string.Empty)
            {
                this.numeroPagina = 1;
                this.MostrarProd();
                this.tableLayoutPanelBotonera.Show();
            }
            else {
                this.BuscaProd();
                this.tableLayoutPanelBotonera.Hide();
            }
        }

        private void btnRefrescar_Click(object sender, EventArgs e)
        {
            this.Refrescar();
        }
    }
}
