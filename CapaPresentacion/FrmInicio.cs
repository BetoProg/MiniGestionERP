﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CapaPresentacion
{
    public partial class FrmInicio : Form
    {
        Productos controlProductos = new Productos();

        public FrmInicio()
        {
            InitializeComponent();
        }

        private void btnProducto_Click(object sender, EventArgs e)
        {
            this.panelContainer.Controls.Clear();
            this.panelContainer.Controls.Add(controlProductos);
        }

        private void btnPedido_Click(object sender, EventArgs e)
        {
            Pedidos controlPedidos = new Pedidos();
            this.panelContainer.Controls.Clear();
            this.panelContainer.Controls.Add(controlPedidos);
        }

        private void btnCategoria_Click(object sender, EventArgs e)
        {
            Categorias controlCategoria = new Categorias();
            this.panelContainer.Controls.Clear();
            this.panelContainer.Controls.Add(controlCategoria);
        }

        private void btnCliente_Click(object sender, EventArgs e)
        {
            Clientes controlClientes = new Clientes();
            this.panelContainer.Controls.Clear();
            this.panelContainer.Controls.Add(controlClientes);
        }

        private void btnTransporte_Click(object sender, EventArgs e)
        {
            Transporte controlTransporte = new Transporte();
            this.panelContainer.Controls.Clear();
            this.panelContainer.Controls.Add(controlTransporte);
        }

        private void btnEmpleados_Click(object sender, EventArgs e)
        {
            Empleados controlEmpleados = new Empleados();
            this.panelContainer.Controls.Clear();
            this.panelContainer.Controls.Add(controlEmpleados);
        }

        private void btnReporte_Click(object sender, EventArgs e)
        {

        }
    }
}
