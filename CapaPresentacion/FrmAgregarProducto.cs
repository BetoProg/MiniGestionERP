﻿using CapaNegocio;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CapaPresentacion
{
    public partial class FrmAgregarProducto : Form
    {
        public FrmAgregarProducto()
        {
            InitializeComponent();
        }

        private void btnCANCELAR_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            Insertar();
        }

        private void Insertar() {
            try
            {
                NProductos.Insertar(this.txtProducto.Text, this.cmbCategoria.Text,
                Convert.ToDecimal(this.txtPrecio.Text), txtDetalles.Text);
                MessageBox.Show("Se ha ingresado correctamente tu Registro");
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
