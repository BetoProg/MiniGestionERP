﻿using CapaDatos;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaNegocio
{
    public class NProductos
    {
        public static DataTable MostrarProd(int regPag, int NumeroPagina)
        {
            return new DProductos().Mostrar(regPag, NumeroPagina);
        }

        public static int Tamanio(int regPag) 
        {
            return new DProductos().TamanioPag(regPag);
        }

        public static DataTable BuscarProd(string nombreBuscado)
        {
            DProductos producto = new DProductos();
            producto.Nombre_Buscado = nombreBuscado;
            return producto.Buscar(producto);
        }

        public static string Insertar(string parNombreProducto, string parNombreCategoria
            , decimal parPrecioUnitario, string pardetalles)
        {
            DProductos producto = new DProductos();
            producto.Nombre_Producto = parNombreProducto;
            producto.Nombre_Categoria = parNombreCategoria;
            producto.Precio_Unitario = parPrecioUnitario;
            producto.Detalles = pardetalles;

            return producto.Insertar(producto);
        }
    }
}
